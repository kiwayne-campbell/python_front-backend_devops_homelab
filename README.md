# python_front-backend_devops_homelab

small python app that has a full devops/SRE concepts and tools

## Devops & SRE home lab

## Description
Devops and SRE are becoming two of the most prevalent disciplines in tech. Both look to find ways to increase efficieny  automation & remove TOIL where possible.  As someone who recewntlx worked in a company that was not making any attempt unfortunately to move towards a development environment that would take advantage of cloud technologxy and other useful development tools i wanted to explore for myself what a home made lab that mnimics a possible DevOps SRE set up.
Overall i want this lab to be able to do the folllowing and also possibly hel others who stumble across this Repo.

- a one stop shop that allows user to simply download the repo with a working Devops flow setup
- They should be able to configure locally and externally: gitlab runners, aws account, kubernetes account etc
- once credentials and local setupds haev been done - they should be abel t push code via this simple frontend application and see chnages occur via cicd pipeline, testing etc and be deployed (essentially mimicing what a liove PROD environment could be running like when person actually works for a company)

## Visuals
- add flow diagram here of what generic setup will look like

## Installation (CURRENT)
For code in current user base
- download repo code
- must install and setup gitlab runners locally.  One done, create two tags: localshell & localrunner (OR whatever you prefer).  If using mac - set the runner up to run with shell env when prompted.  https://docs.gitlab.com/runner/install/
- install docker on local machine

## Installation (FUTURE)
this repo will then add two further tools for hopefully a small and basic devops/ SRE homelab: 
Connection to AWS & also Kubernetes

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Roadmap
- add Connection to AWS ***
- add Kubernetes (for scale) ***
- add python testing unit correctly and output via artifacts
- add loadbalancer setup for aws
- add IAAC tool -  terraform
- add monitoring tool: prometheus & Grafana
-


## Roadmap BONUS 
- find traffic testing tool to simulate visitors to an application
- clean up folder structure - so can identifiy easily elements in DevOps/SRE flow e.g cici_flow, deployment_testing, containerizaton--> kubernetes_scaling, dockerization etc
- add UAT & PROD environments
- enhance security

## Authors and acknowledgment
This homelab was made possible thanks to this udemy course: [UDEMY gitlab-cicd-course](https://www.udemy.com/course/gitlab-cicd-course/) & [freecode-camp - CICD]https://www.youtube.com/watch?v=PGyhBwLyK2U - the rest i slavaged from lots of googling! :)

## Project status
in use still
